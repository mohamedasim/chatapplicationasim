// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCbiMFRXxWHhpa_4k9NFvetutJ-J9ouLvI",
    authDomain: "chat-app-66a53.firebaseapp.com",
    databaseURL: "https://chat-app-66a53.firebaseio.com",
    projectId: "chat-app-66a53",
    storageBucket: "chat-app-66a53.appspot.com",
    messagingSenderId: "470272382993",
    appId: "1:470272382993:web:ac82d48a6e45551dbdf9b0",
    measurementId: "G-MLV37LE2FJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
