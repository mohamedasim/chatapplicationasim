import {
  Injectable
} from '@angular/core';
import {
  AngularFirestore
} from '@angular/fire/firestore';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  ToastrService
} from 'ngx-toastr';
import {
  Observable
} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  logdata: any;
  uname: any;
  pas: any;
  localseluser: any;
  selectuser: any;
  regdata:any;
  logcheck: any;
  fromname: string;
  messagelist: any;
  public logname;
  constructor(private fire: AngularFirestore, private router: Router, private route: ActivatedRoute, private toastr: ToastrService) {}
  getlocalloginname() {
    let log = JSON.parse(localStorage.getItem('data'));
    return log;
  }
  getlocalfromname() {
    let from = JSON.parse(localStorage.getItem('from'));
    return from;
  }
  registeration(register) {
    return this.fire.collection('user').add(register);
  }
  updateregistereddata(obj, id) {
    debugger
    return this.fire.doc('user/' + id).update(obj);
  }
  getallregistrationlist() {
    return this.fire.collection('user').snapshotChanges();
  }
  uploadimage(img) {
    return this.fire.collection('user').add(img);
  }
  loginname(loginname) {
    debugger
    this.logname = loginname;
    let tabledata = this.logname
    let data = JSON.stringify(tabledata)
    localStorage.setItem('data', data)
    return this.logname
  }
  sendAllMsg(record) {
    return this.fire.collection('chat').add(record);
  }

  getallmsgslist() {
    debugger
    return this.fire.collection('chat').snapshotChanges();
  }
  updatemessage(id,record){debugger
    return this.fire.doc('chat/'+id).update(record);
     }
  logout() {
    this.router.navigate(['/'], {
      relativeTo: this.route
    });
    //localStorage.clear();
  }
  deletemessages(Id) {
    debugger
    return this.fire.doc('chat/' + Id).delete();
  }

  localselecteduser(seluser) {
    debugger
    this.localseluser = seluser;
    let locseluser = this.localseluser;
    //let locseluser = JSON.stringify(tabledata)
    localStorage.setItem('locseluser', locseluser)
    return this.localseluser;
  }
  locselusers() {
    let a = JSON.parse(localStorage.getItem('locseluser'));
    return a;
  }
  showHTMLMessage(message, title) {
    this.toastr.show(message, title, {
      enableHtml: true
    })
  }
   
}
