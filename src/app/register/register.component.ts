import {
  Component,
  OnInit
} from '@angular/core';
import {
  Register
} from '../register.interface';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DataService
} from '../data.service';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register: Register[];
  registercheck: any;
  message: string;
  usercheck: string;
  uniname: any;
  unique: any;
  formdata = new FormGroup({
    name: new FormControl('', Validators.required),
    username: new FormControl('', Validators.required),
    pass: new FormControl('', Validators.required),
    repass: new FormControl('', Validators.required),
  });
  constructor(private service: DataService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.service.getallregistrationlist().subscribe(data => {
      debugger;
      this.registercheck = data.map(e => {
        return {
          id: e.payload.doc.id,
          username: e.payload.doc.data()['username'],
          pass: e.payload.doc.data()['pass'],
        }
      });
    });
  }

  submitdata() {
    debugger;
    let pw = this.formdata.value['pass'];
    let rpw = this.formdata.value['repass'];
    let uname = this.formdata.value['username'];
    this.register = this.registercheck;
    let arr = this.register
    for (let index = 0; index < arr.length; index++) {
      if (arr[index].username == uname) {
        this.usercheck = "Username Already Taken Try Another!";
        this.uniquename(arr[index].username);
        break;
      }
    }
    let uniquelocal = this.getlocaluniquename();
    if (pw == rpw && uniquelocal != uname) {
      alert('password matched');
      this.register = this.formdata.value
      this.service.registeration(this.register).then(res => {
        console.log(res);
        this.message = "your registration was success!";
        this.router.navigate(['/login'], {
          relativeTo: this.route
        });
      }).catch(error => {
        console.log(error);
      });
      this.formdata.reset();
    } else {
      alert('password not matched or the username is already taken')

    }
  }

  getlocaluniquename() {
    this.uniname = JSON.parse(localStorage.getItem('uniquedata'));
    return this.uniname;
  }
  uniquename(uniquename) {
    debugger
    this.unique = uniquename;
    let tabledata = this.unique;
    let uniquedata = JSON.stringify(tabledata)
    localStorage.setItem('uniquedata', uniquedata)
    return this.unique
  }
}
