import {
  AfterViewChecked,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms'
import {
  DataService
} from '../data.service';
import {
  ToastrService
} from 'ngx-toastr';
import {
  Chat,
  Register,
  ChatByOrder,
  Users
} from '../register.interface';
import {
  ConnectionService
} from 'ng-connection-service';
import {
  Observable
} from 'rxjs';
import {
  AngularFirestore
} from '@angular/fire/firestore';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask
} from '@angular/fire/storage';
import {
  map,
  finalize
} from 'rxjs/operators';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
// ,AfterViewChecked
export class MessageComponent implements OnInit {
  message: string;
  chat: Chat[];
  status = "offline";
  isconnected = false;
  atoastr: any;
  btoastr: any;
  msgs: string;
  logname: any;
  loginlogoutcheck: any;
  showmessage = false;
  isread=false;
  view = false;
  messagelist: any;
  localchatsbyorder: any;
  chatsbyorder: ChatByOrder[];
  date: string;
  users: Users[];
  rightclickevents = false;
  registeredUser: Register[];
  inputId: any;
  updaterecord = false;
  to: string;
  test: any;
  selectuser: any;
  localfromtod: any;
  msg1: any;
  selecteduser: any;
  localseluser: any;
  getmsg: any;
  registeredname: any;
  // ***********fire photo upload  code******************
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable < number > ;
  downloadURL: any;
  uploadState: Observable < string > ;
  formdata = new FormGroup({
    msgs: new FormControl('', Validators.required),
    // to: new FormControl('',Validators.required)
  });
  @ViewChild('scroll') private myScrollContainer: ElementRef;
  constructor(private service: DataService, private toastr: ToastrService, private afStorage: AngularFireStorage, private fire: AngularFirestore) { }
  ngOnInit(): void {
    debugger
    //this.getmsgs();
    this.getlogname();
    this.getregisteredusers();
    this.locselusers();
    this.scrollToBottom();
  }
  // ngAfterViewChecked(){debugger
  //   this.scrollToBottom();
  // }
  showHtmlToaster(a, b) {
    debugger
    this.atoastr = a;
    this.btoastr = b;
    this.service.showHTMLMessage(a,b);
  }
  sendmsg() {
    debugger

    if (!this.updaterecord) {
      this.msgs = this.formdata.value['msgs'];
      this.to = this.selectuser;
      this.date = new Date().toLocaleString();
      let record = {};
      record['Messages'] = this.msgs;
      record['To'] = this.to;
      record['From'] = this.logname;
      record['Date'] = this.date;
      record['isread']=this.isread;
      this.service.sendAllMsg(record).then(res => {
        console.log(res);
        this.message = "your msg have received.";
      }).catch(error => {
        console.log(error);
      });
    } else {
      this.msgs = this.formdata.value['msgs'];
      let uprecord = {};
      uprecord['Messages'] = this.msgs;
      this.service.updatemessage(this.inputId, uprecord);
      this.updaterecord = false;
    }
    this.formdata.reset();
    this.getmsgs();
    this.scrollToBottom();
  }
  getlogname() {
    debugger;
    this.logname = this.service.getlocalloginname();
  }
  uploadimage(event) {
    this.service.uploadimage(event);
  }
  getregisteredusers() {
    this.service.getallregistrationlist().subscribe(data => {
      this.registeredUser = data.map(e => {
        return {
          id: e.payload.doc.id,
          name: e.payload.doc.data()['name'],
          pass: e.payload.doc.data()['pass'],
          repass: e.payload.doc.data()['repass'],
          username: e.payload.doc.data()['username'],
          logintime: e.payload.doc.data()['logintime'],
          logouttime: e.payload.doc.data()['logouttime'],
          isread: e.payload.doc.data()['isread'],
        };
      });
      this.users = this.registeredUser.filter(e => {
        return e.logouttime || e.username != this.logname
      });

    });
  }
  getmsgs() {
    debugger
    this.service.getallmsgslist().subscribe(data => {debugger
      this.chat = data.map(e => {
        return {
          Id: e.payload.doc.id,
          From: e.payload.doc.data()['From'],
          To: e.payload.doc.data()['To'],
          Date: e.payload.doc.data()['Date'],
          Messages: e.payload.doc.data()['Messages'],
          Isread:e.payload.doc.data()['isread'],
        };
      }).filter(e => {
        return ((e.From == this.selecteduser) && (e.To == this.logname) || (e.From == this.logname) && (e.To == this.selecteduser))
      }).sort((a, b) => {
        return <any > new Date(a.Date) - < any > new Date(b.Date);
      });
      this.checkStatusOfMessage();
    });
    this.scrollToBottom();
  }
  id: any;
  logout() {
    debugger
    let arr = this.registeredUser;
    for (let index = 0; index < arr.length; index++) {
      if (arr[index].username == this.logname) {
        this.id = arr[index].id;
        let obj = {};
        obj['logouttime'] = new Date().toLocaleString();
        this.service.updateregistereddata(obj, this.id);
        break;
      }
    }
    this.service.logout();
  }
  visibleusers(){
    this.showmessage=false;
  }
  onSelectingUser(user) {
    this.showmessage = true;
    debugger
    this.service.localselecteduser(user['username']);
    this.selecteduser = this.locselusers();
    // this.selectId=user['id']
    let obj={
      isread:false,
    }
    this.getmsgs();
    this.scrollToBottom();
  }
  selectId:any;
  checkStatusOfMessage(){debugger
    let arr =this.chat

       for (let index = 0; index < arr.length; index++) {
         if(arr[index].From==this.selecteduser){
          let obj={
            isread:true,
          }
          this.selectId=arr[index].Id
          // alert(this.selectId)
          if(arr[index].Isread==false){
          this.service.updatemessage(this.selectId,obj)
        }
         }
    }

  }
  copyMessage(val: string) {
    this.rightclickevents = true;
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  scrollToBottom(): void {
    debugger
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {}
  }
  playaudio() {
    debugger
    let tone = new Audio;
    tone.src = "assets/skype_message.mp3";
    tone.load()
    tone.play();
  }
  locselusers() {
    this.selectuser = localStorage.getItem('locseluser');
    return this.selectuser;
  }
  deletemessage(chat) {
    debugger
    this.service.deletemessages(chat);
    this.getmsgs();
  }
  editMessage(msg, id) {
    this.formdata.patchValue({
      msgs:msg,
    })
    this.updaterecord = true;
    this.inputId = id;
  }
}
