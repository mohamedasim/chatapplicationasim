export interface Register{
    id:string;
    name:string,
    username:string,
    pass:string,
    repass:string,
    logintime:string,
    logouttime:string,
    isread:boolean,
}
export interface Chat{
    To:string;
    From:string;
    Messages:string;
    Date:string;
    Id:string;
    Isread:boolean;
}
 export interface Users{
     username:string;
     logouttime:string;
     id:string;
     isread:boolean;
 }
 export interface SelectedUser{
     username:string;
     name:string;
     pass:string;
     repass:string;
 }
 export interface ChatByOrder{
    To:string;
    From:string;
    Messages:string;
    Date:string;
    Id:string;
 }