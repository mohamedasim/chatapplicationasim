import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"message",component:MessageComponent},
  {path:"register",component:RegisterComponent},
  {path:"", redirectTo:'login',pathMatch:"full"},  
  {path:"**",component:LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
