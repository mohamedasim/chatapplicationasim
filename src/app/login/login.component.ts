import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DataService
} from '../data.service';
import {
  ActivatedRoute,
  Router
} from '@angular/router'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  logdata: any;
  loginname: string;
  logintime: string;
  logouttime = "";
  missmatch: string;
  formdata = new FormGroup({
    username: new FormControl('', Validators.required),
    pass: new FormControl('', Validators.required)
  });
  constructor(private service: DataService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.service.getallregistrationlist().subscribe(data => {
      debugger;
      this.logdata = data.map(e => {
        return {
          id: e.payload.doc.id,
          username: e.payload.doc.data()['username'],
          pass: e.payload.doc.data()['pass'],
        }
      });
    });
  }
  id: any;
  submitdata() {
    debugger;
    let username = this.formdata.value['username'];
    let pass = this.formdata.value['pass'];
    this.logintime = new Date().toLocaleString();
    let obj = {};
    obj['logintime'] = this.logintime;
    obj['logouttime'] = this.logouttime;

    let arr = this.logdata;
    for (let index = 0; index < arr.length; index++) {
      if (arr[index].username == username && arr[index].pass == pass) {
        this.loginname = arr[index].username;
        this.id = arr[index].id;
        this.service.loginname(this.loginname);
        this.service.updateregistereddata(obj, this.id);
        console.log(this.loginname);
        this.router.navigate(['/message'], {
          relativeTo: this.route
        });
        break;
      } else {
        this.missmatch = "User And Login Not Matched";
      }
    }
  }

}
